# NOTATKA - checkpoint 2

## Architektura systemu

- środowisko docker-compose, oddzielne hosty agenta, aplikacji oraz loggera
- siec bridged
  
## Wykorzystywane biblioteki

### Aplikacja

- click - interfejs CLI
- importlib - realizacja architektury pluginów (zapewniona rozszerzalnośc systemu oraz odpowiednia separacja komponentów do obsługi
  róznych typów plików z logami)
- requests - obsługa ządań HTTP wysyłanych na serwer po stronie agenta/zdalnego loggera
- ScaPy - analiza plików PCAP
- re - obsługa wyrazeń regularnych
- os - obsługa wykonania poleceń systemowych (np. grep)
- pandas - analiza danych, DataFrame

### Zdalny logger alertów

- FastAPI - fasada do obsługi serwera HTTP
- Uvicorn - serwer HTTP, dokumentacja Swagger
- logging - logowanie alertów z predefiniowaną konfiguracją umozliwiającą łatwy odczyt informacji

### Agent

- FastAPI - fasada do obsługi serwera HTTP
- Uvicorn - serwer HTTP, dokumentacja Swagger
- ScaPy/tcpdump - przechwytywanie pakietów ruchu sieciowego na polecenie aplikacji

## Analiza plików PCAP offline
- alertor pcap FILENAME --bpf BPF
- mozliwe przekazanie tekstu w formacie BPF do odfiltrowania pakietów

## Analiza plików TXT
- alertor txt FILENAME --grep --regex EXPR
- wyrazenie regularne jako argument operacji grep (wywoływanej przez bibliotekę os)
- brak obecności flagi implikuje tryb przetwarzania na bazie przekazanego wyrazenia regularnego

## Deklaracja reguł detekcji
- ruch z nieznanego hosta w równych odstępach czasu
- nietypowe porty ruchu wychodzącego
- ruch z adresów IP bez serwera DNS
- charakterystyka czasowa hosta

## Zdalna kontrola agenta
- polecenia powłoki systemowej przekazywane jako parametr payloadu zapytania
- akcje przechwytywania pakietów oraz przesyłu plików z logami określona i oskryptowana po stronie agenta
