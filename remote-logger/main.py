from fastapi import FastAPI
from pydantic import BaseModel
from logging.config import dictConfig
import logging

EDR = 5
logging.addLevelName(EDR, 'ALERTOR')

def edr(self, message, *args, **kws):
    self.log(EDR, message, *args, **kws) 
logging.Logger.edr = edr

log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(levelprefix)s %(asctime)s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",

        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    },
    "loggers": {
        "alert-logger": {"handlers": ["default"], "level": "ALERTOR"},
    },
}

dictConfig(log_config)

logger = logging.getLogger('alert-logger')

class LogItem(BaseModel):
    content: str

app = FastAPI()

@app.post("/edr/log")
def write__edr_log(log: LogItem):
    logger.edr(log.content)
