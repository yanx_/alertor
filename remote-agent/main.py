from fastapi import FastAPI
from pydantic import BaseModel
from logging.config import dictConfig
from starlette.responses import FileResponse
from starlette.background import BackgroundTask
import logging
from scapy.all import *
import subprocess
import json
from shutil import make_archive


app = FastAPI()

class ScanConfig(BaseModel):
    iface: str
    timeout: int
    
@app.post("/edr/scan")
def scan_network_traffic(config: ScanConfig):
    packets = sniff(iface=config.iface, timeout=config.timeout)
    script_dir = os.path.dirname(__file__)
    if not os.path.exists(os.path.join(script_dir, 'tmp')):
        os.makedirs(os.path.join(script_dir, 'tmp'))
    wrpcap("tmp/response.pcap", packets)
    return FileResponse(path="tmp/response.pcap", background=BackgroundTask(os.remove, "tmp/response.pcap"))

@app.get("/edr/logs")
def zip_logs():
    script_dir = os.path.dirname(__file__)
    if not os.path.exists(os.path.join(script_dir, 'tmp')):
        os.makedirs(os.path.join(script_dir, 'tmp'))
    make_archive(os.path.join(script_dir, 'tmp', 'response'), 'zip', '/var/log')
    return FileResponse(path="tmp/response.zip", background=BackgroundTask(os.remove, "tmp/response.zip"))

@app.post("/edr/exec")
def execute_command(command: str):
    return os.popen(command).read()
