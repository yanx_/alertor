import importlib
import logging
import os

logger = logging.getLogger('alertor')

class Alertor:
    def __init__(self):
        self.plugins = {}

    def _import_plugins(self, dirname):
        plugins_in_dir = os.listdir(dirname)
        if len(plugins_in_dir) > 0:
            for plugin in plugins_in_dir:
                plugin_name = os.path.splitext(plugin)[0]
                if not is_not_plugin(plugin_name) and plugin_name not in self.plugins:
                    self.plugins[plugin_name] = importlib.import_module('.' + plugin_name,dirname).Plugin()
                    logger.info(plugin_name + ' plugin loaded...')
        else:
            raise Exception("No plugins provided")

    def import_plugin(self, plugin_name, dirname):
        try:
            self.plugins[plugin_name] = importlib.import_module('.' + plugin_name,dirname).Plugin()
            logger.info(plugin_name + ' plugin loaded...')
        except:
            raise Exception("Failed to load plugin")

def is_not_plugin(filename):
    if filename.startswith('__'):
        return True
    return False