import logging
import time
import os

logging.basicConfig(level = logging.INFO, format='%(levelname)s %(asctime)s - %(message)s')

def setup_custom_logger(name):
    timestr = time.strftime("%Y_%m_%d-%H_%M_%S")
    formatter = logging.Formatter(fmt='%(levelname)s %(asctime)s - %(message)s')

    if not os.path.exists("logs"):
        os.mkdir("logs")

    file_handler = logging.FileHandler(f"logs/ALERTOR_{timestr}.log")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.addHandler(file_handler)
    return logger