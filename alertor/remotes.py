from abc import ABC, abstractmethod
import json
import requests
from datetime import datetime
import os
import logging

logger = logging.getLogger('alertor')

class RemoteAction(ABC):
    def __init__(self, host, port, config=None, command=None):
        self.host = host
        self.port = port
        self.config = config
        self.command = command

    @abstractmethod
    def perform(self):
        pass

class RemoteScanningAction(RemoteAction):
    def perform(self):
        script_dir = os.path.dirname(__file__)
        with open(os.path.join(script_dir, self.config)) as conf_file:
            c = json.load(conf_file)
        logger.info(f"Scanning on remote, this will take about {c['timeout']} seconds...")
        response = requests.post(f"http://{self.host}:{self.port}/edr/scan", json=c)
        pcap_filename = datetime.now().strftime('Scan_%H:%M:%S_%d-%m-%Y.pcap')
        create_result_dir('scans', script_dir)
        with open(os.path.join(script_dir, 'scans', datetime.now().strftime('Scan_%H:%M:%S_%d-%m-%Y.pcap')), "wb") as out:
            out.write(response.content)
        logger.info(f"Scan successful, results saved to scans/{pcap_filename}")

class RemoteLoggingAction(RemoteAction):
    def perform(self):
        script_dir = os.path.dirname(__file__)
        logger.info("Downloading logs from /var/log")

        response = requests.get(f"http://{self.host}:{self.port}/edr/logs")
        logs_filename = datetime.now().strftime('Logs_%H:%M:%S_%d-%m-%Y.zip')
        create_result_dir('logs', script_dir)
        with open(os.path.join(script_dir, 'logs', logs_filename), "wb") as out:
            out.write(response.content)
        logger.info(f"Logs downloaded, results saved to logs/{logs_filename}")

class RemoteExecutionAction(RemoteAction):
    def perform(self):
        logger.info(f"Executing remote command '{self.command}'")
        payload = {'command': self.command}
        response = requests.post(f"http://{self.host}:{self.port}/edr/exec", params=payload)
        for line in response.text.split('\\n'):
            logger.info(line)

class RemoteActionFactory:
    def create_action(action, host, port, config=None, command=None):
        if action == 'scan':
            return RemoteScanningAction(host, port, config)
        if action == 'logs':
            return RemoteLoggingAction(host, port)
        if action == 'exec':
            return RemoteExecutionAction(host, port, command=command)
        if action == 'ifconfig':
            return RemoteExecutionAction(host, port, command='ifconfig')
        else:
            raise Exception("Provided command could not be handled")


def create_result_dir(dirname: str, location: str):
    if not os.path.exists(os.path.join(location, dirname)):
            os.makedirs(os.path.join(location, dirname))

def send_message_to_remote_logger(host, port, payload):
    url = f"http://{host}:{port}/edr/log"
    requests.post(url, json=payload)