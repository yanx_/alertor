class NetworkPacketUnit:
    def __init__(self, src_addr=None, dst_addr=None, src_port=None, dst_port=None, src_hwaddr=None, dst_hwaddr=None, tl_payload=None):
        self.src_addr = src_addr
        self.dst_addr = dst_addr
        self.src_port = src_port
        self.dst_port = dst_port
        self.src_hwaddr = src_hwaddr
        self.dst_hwaddr = dst_hwaddr
        self.tl_payload = tl_payload
    
    def __repr__(self):
        return f"""<IP src: {self.src_addr}, IP dst: {self.dst_addr},
        src port: {self.src_port}, dst port: {self.dst_port},
        src MAC addr: {self.src_hwaddr}, dst MAC addr: {self.dst_hwaddr},
        transport layer payload: {self.tl_payload}>"""