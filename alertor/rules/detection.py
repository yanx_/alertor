import logging
from typing import List, Tuple
from rules.entity import NetworkPacketUnit
import remotes

logger = logging.getLogger('alertor')

class DetectionRulesSet:

    def find_ssh_attempts(units: List[NetworkPacketUnit], verbose: bool) -> Tuple[str, str]:
        logger.info("Detecting SSH attempts...")
        condition = False
        attempts = 0
        for unit in units:
            if unit.dst_port == 22 or unit.src_port == 22:
                condition = True
                attempts = attempts + 1
        return "remote", DetectionRulesSet.log_detection_results("SSH connection", condition, attempts)
        
    def find_path_traversal_attempts(units: List[NetworkPacketUnit], verbose: bool) -> Tuple[str, str]:
        logger.info("Detecting path traversal attack attempts...")
        condition = False
        attempts = 0
        for unit in units:
            if "../" in unit.tl_payload:
                condition = True
                attempts = attempts + 1
                if verbose:
                    logger.warn(unit.tl_payload)
        return "remote", DetectionRulesSet.log_detection_results("Path Traversal", condition, attempts)

    def find_xss_attempts(units: List[NetworkPacketUnit], verbose: bool) -> Tuple[str, str]:
        logger.info("Detecting XSS attact attempts...")
        condition = False
        attempts = 0
        for unit in units:
            if "<script" in unit.tl_payload:
                condition = True
                attempts = attempts + 1
                if verbose:
                    logger.warn(unit.tl_payload)
        return "remote", DetectionRulesSet.log_detection_results("XSS", condition, attempts)

    def find_sql_injection_attempts(units: List[NetworkPacketUnit], verbose: bool) -> Tuple[str, str]:
        logger.info("Detecting SQL injection attack attempts...")
        condition = False
        attempts = 0
        for unit in units:
            if "SELECT+@@VERSION" in unit.tl_payload or "'OR'" in unit.tl_payload:
                condition = True
                attempts = attempts + 1
                if verbose:
                    logger.warn(unit.tl_payload)
        return "remote", DetectionRulesSet.log_detection_results("SQL injection", condition, attempts)

    def find_rule_by_alias(rule: str, units: List[NetworkPacketUnit], verbose: bool) -> Tuple[str, str]:
        if rule == 'path_traversal':
            return DetectionRulesSet.find_path_traversal_attempts(units, verbose)
        if rule == 'ssh':
            return DetectionRulesSet.find_ssh_attempts(units, verbose)
        if rule == 'xss':
            return DetectionRulesSet.find_xss_attempts(units, verbose)
        if rule == 'sqli':
            return DetectionRulesSet.find_sql_injection_attempts(units, verbose)

    def execute_all_rules(units: List[NetworkPacketUnit], verbose: bool):
        action, description = DetectionRulesSet.find_path_traversal_attempts(units, verbose)
        DetectionRulesSet.handle_detection_results(action, description)

        action, description = DetectionRulesSet.find_ssh_attempts(units, verbose)
        DetectionRulesSet.handle_detection_results(action, description)

        action, description = DetectionRulesSet.find_xss_attempts(units, verbose)
        DetectionRulesSet.handle_detection_results(action, description)

        action, description = DetectionRulesSet.find_sql_injection_attempts(units, verbose)
        DetectionRulesSet.handle_detection_results(action, description)
    
    def log_detection_results(attack_name: str, found: bool, attempts: int):
        if found:
            return f"[+] {attack_name}: Found {attempts} attempt(s)\n"
        else:
            return None

    def handle_detection_results(action: str, description: str) -> None:
        if description != None:
            logger.warning(description)
        if action == "remote" and description != None:
            remotes.send_message_to_remote_logger("localhost", "3333", {"content": description})