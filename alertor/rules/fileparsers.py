from abc import ABC, abstractmethod
import os
import json
from scapy.all import *
from urllib.parse import unquote
from rules.entity import NetworkPacketUnit

logger = logging.getLogger('alertor')

class FileProcessor(ABC):
    def __init__(self, filename):
        self.filename = filename

    @abstractmethod
    def get_units(self):
        pass

class PcapProcessor(FileProcessor):
    def get_units(self):
        units = []
        pcap_flow = rdpcap(self.filename)
        # TODO: write to the log file
        for packet in pcap_flow:
            try:
                src_hwaddr = dst_hwaddr = None
                src_addr = dst_addr = None
                if IP in packet:
                    src_addr, dst_addr = packet[IP].src, packet[IP].dst
                if UDP in packet:
                    src_port, dst_port = packet[UDP].sport, packet[UDP].dport
                    tl_payload = bytes(packet[UDP].payload).decode("utf-8")
                if TCP in packet:
                    src_port, dst_port = packet[TCP].sport, packet[TCP].dport
                    tl_payload = bytes(packet[TCP].payload).decode("utf-8")
                if Ether in packet:
                    src_hwaddr, dst_hwaddr = packet[Ether].src, packet[Ether].dst 
                unit = NetworkPacketUnit(src_addr, dst_addr, src_port, dst_port, src_hwaddr, dst_hwaddr, unquote(tl_payload))
                units.append(unit)   
            except Exception as e:
                logger.error(e)
        return units

class JsonProcessor(FileProcessor):
    def get_units(self):
        units = []
        with open(self.filename) as json_file:
            packets = json.load(json_file)
            for pkt in packets:
                entry = pkt["_source"]
                is_ip = entry["layers"]["frame"]["frame.protocols"].find("ip")
                is_ipv6 = entry["layers"]["frame"]["frame.protocols"].find("ipv6")
                is_tcp = entry["layers"]["frame"]["frame.protocols"].find("tcp")
                is_udp = entry["layers"]["frame"]["frame.protocols"].find("udp")
                is_tls = entry["layers"]["frame"]["frame.protocols"].find("tls")
                is_eth = entry["layers"]["frame"]["frame.protocols"].find("eth")

                eth_src = eth_dst = None
                if is_eth != -1:
                    eth_src, eth_dst = JsonProcessor.get_eth_addresses(entry)

                if is_ip != -1 and is_ipv6 == -1:
                    ip_src, ip_dst = JsonProcessor.get_ip_address(entry)

                if is_ipv6 != -1:
                    ip_src, ip_dst = JsonProcessor.get_ipv6_address(entry)

                if is_tcp != -1:
                    srcport, dstport, tl_payload = JsonProcessor.get_tcp_data(entry)
                elif is_udp != -1:
                    srcport, dstport, tl_payload = JsonProcessor.get_udp_data(entry)

                if is_tls != -1:
                    tl_payload = "656e636f646564"
                unit = NetworkPacketUnit(ip_src, ip_dst,
                    srcport, dstport,
                    eth_src, eth_dst,
                    unquote(bytes.fromhex(tl_payload).decode('latin')))
                units.append(unit)
        return units


    def get_eth_addresses(entry):
        eth_src = eth_dst = None
        eth_layer = entry["layers"]["eth"]
        eth_src, eth_dst = eth_layer["eth.src"], eth_layer["eth.dst"]
        return eth_src, eth_dst
    
    def get_ip_address(entry):
        ip_src = ip_dst = None
        ip_layer = entry["layers"]["ip"]
        ip_src = ip_layer["ip.src"]
        ip_dst = ip_layer["ip.dst"]
        return ip_src, ip_dst

    def get_ipv6_address(entry):
        ip_src = ip_dst = None
        ip_layer = entry["layers"]["ipv6"]
        ip_src = ip_layer["ipv6.src"]
        ip_dst = ip_layer["ipv6.dst"]
        return ip_src, ip_dst

    def get_tcp_data(entry):
        srcport = dstport = None
        transport_layer = entry["layers"]["tcp"]
        srcport, dstport = transport_layer["tcp.srcport"], transport_layer["tcp.dstport"]
        try:
            tl_payload = transport_layer["tcp.payload"].replace(":", "")
        except:
            tl_payload = " "
        return srcport, dstport, tl_payload

    def get_udp_data(entry):
        srcport = dstport = None
        transport_layer = entry["layers"]["udp"]
        srcport, dstport = transport_layer["udp.srcport"], transport_layer["udp.dstport"]
        try:
            tl_payload = transport_layer["udp.payload"].replace(":", "")
        except:
            tl_payload = " "
        return srcport, dstport, tl_payload
            

class EvtxProcessor(FileProcessor):
    def get_units(self):
        raise NotImplementedError

class XmlProcessor(FileProcessor):
    def get_units(self):
        raise NotImplementedError


class TxtProcessor(FileProcessor):
    def get_units(self):
        units = []
        with open(self.filename,"r") as file:
                for line in file:
                    src_addr = line.split(" - ")[0]
                    tl_payload = line.split("] ")[1]
                    units.append(NetworkPacketUnit(src_addr=src_addr, tl_payload=unquote(tl_payload)))
        return units

class FileParserFactory:
    def create_file_parser_by_file_extension(filename):
        fname, fext = os.path.splitext(filename)
        if fext == ".pcap":
            return PcapProcessor(filename)
        if fext == ".json":
            return JsonProcessor(filename)
        if fext == ".txt":
            return TxtProcessor(filename)
        if fext == ".log":
            return TxtProcessor(filename)
        if fext == ".xml":
            return XmlProcessor(filename)
        if fext == ".evtx":
            return EvtxProcessor(filename)