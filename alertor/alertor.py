import click
from pyfiglet import Figlet
from internal import core
from rules import fileparsers
from rules import detection
import remotes
import log
import os

logger = log.setup_custom_logger('alertor')

@click.group(chain=True)
@click.option('--logging', '-l', default=True, help="Flag for switching logger on/off")
@click.pass_context
def cli(ctx, logging):
    """Command line tool for analysing log files"""
    f = Figlet(font='slant')
    print(f.renderText('alertor'))
    logger.propagate = logging
    ctx.obj = core.Alertor()

@cli.command(name='dirs')
@click.argument('dirs', nargs=-1)
@click.pass_context
def directory_scanning(ctx, dirs):
    """Directory content analysis"""
    logger.info("Scanning directory...")
    for dir in dirs:
        if os.path.isdir(dir):
            for filename in os.listdir(dir):
                f = os.path.join(dir, filename)
                if os.path.isfile(f):
                    fname, fext = os.path.splitext(f)
                    if len(fext) > 1:
                        try:
                            fext_plugin = f"{fext[1:]}s"
                            ctx.obj.import_plugin(fext_plugin, 'plugins')
                            get_plugin_ref(fext_plugin).print(f)
                        except Exception as e:
                            logger.error(f"Provided {fext} extension is not supported")
                elif os.path.isdir(f):
                    logger.info(f"{fname} is a directory")

@cli.command(name='xml')
@click.argument('file', nargs=1)
@click.pass_context
def xml_analyse(ctx, file):
    """XML file analysis"""
    ctx.obj.import_plugin('xmls', 'plugins')
    get_plugin_ref('xmls').print_xml(file)

@cli.command(name='json')
@click.argument('file', nargs=1)
@click.pass_context
def json_analyse(ctx, file):
    """JSON file analysis"""
    ctx.obj.import_plugin('jsons', 'plugins')
    get_plugin_ref('jsons').print_json(file)

@cli.command(name='pcap')
@click.argument('input', nargs=1)
@click.argument('bpf', required = False)
@click.pass_context
def pcap_analyse(ctx, input, bpf):
    """PCAP file analysis"""
    ctx.obj.import_plugin('pcaps', 'plugins')
    get_plugin_ref('pcaps').print_pcap(input, bpf)

@cli.command(name='txt')
@click.argument('input', nargs=-1)
@click.argument('regex', nargs=1)
@click.pass_context
def txt_analyse(ctx, input, regex):
    """TXT file analysis"""
    ctx.obj.import_plugin('txts', 'plugins')
    get_plugin_ref('txts').grep(input, regex)

@cli.command(name="evtx")
@click.argument('input', nargs=1)
@click.pass_context
def evtx_analyse(ctx, input):
    """EVTX log file analysis"""
    ctx.obj.import_plugin('evtxs', 'plugins')
    get_plugin_ref('evtxs').open_evtx(input)

@cli.command(name="detect")
@click.argument('file', nargs=1)
@click.argument('rule', default='all', nargs=1)
@click.option('-v', '--verbose', is_flag=True)
@click.pass_context
def detect_threats(ctx, file, rule, verbose):
    """Available rules: path_traversal, ssh, xss, sqli"""
    logger.info(f"Detection on {file}")
    processor = fileparsers.FileParserFactory.create_file_parser_by_file_extension(file)
    units = []
    try:
        units = processor.get_units()
    except NotImplementedError:
        logger.error("Parser not implemented")
        return

    if rule == 'all':
        detection.DetectionRulesSet.execute_all_rules(units, verbose)
    
    else:
        action_alert, description = detection.DetectionRulesSet.find_rule_by_alias(rule, units, verbose)
        if description != None:
            logger.warning(description)
        if action_alert == "remote":
            remotes.send_message_to_remote_logger("localhost", "3333", {"content": description})



@cli.command(name='remote')
@click.argument('host', nargs=1)
@click.argument('action', nargs=1)
@click.option('--config', default="resources/default_config.json", help="Path to JSON configuration file for the scan command")
@click.option('--port', '-p', default="4444", help="Remote host port number, default is 4444")
@click.option('--command', '-c', default="pwd", help="Command string for the exec command")
def remote_action(host, action, config, port, command):
    """Available actions: scan, logs, ifconfig, exec"""
    try:
        remotes.RemoteActionFactory.create_action(action, host, port, config, command).perform()
    except Exception as e:
        logger.error("Failed to connect with agent")




@click.pass_context
def get_plugin_ref(ctx, plugin_name):
    return ctx.obj.plugins[plugin_name]


if __name__ == '__main__':
    cli()