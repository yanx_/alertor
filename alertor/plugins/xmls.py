import xml.etree.ElementTree as ET
import logging

logger = logging.getLogger('alertor')

class Plugin:
    def print_xml(self, xmlfilepath):
        logger.info("Reading XML file...")
        if xmlfilepath[-4:] == '.xml':
            f = open(xmlfilepath, 'r')
            logger.info(f.read())
        else:
            raise Exception("Wrong file format")

    def print(self, filename):
        self.print_xml(filename)

            