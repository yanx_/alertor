import Evtx.Evtx as evtx
import Evtx.Views as e_views
import logging

logger = logging.getLogger('alertor')

class Plugin:
    def open_evtx(self, input_file):
        logger.info("Reading EVTX file...")
        if input_file[-5:] == '.evtx':
            with evtx.Evtx(input_file) as log:
                r = log.get_record(1)
                if r is None:
                    print("error: record not found")
                    return -1
                else:
                    logger.info(e_views.evtx_template_readable_view(r.root()))

    def print(self, filename):
        self.open_evtx(filename)
