import re
import logging

logger = logging.getLogger('alertor')


class Plugin:
    def grep(self, files, expr):
        logger.info("Reading TXT file(s)...")
        for filename in files:
            self.grep_file_content(filename, expr)
            

    def grep_file_content(self, filename, expr):
        with open(filename,"r") as file:
                for line in file:
                    if re.search(expr, line):
                        logger.info(line)
    
    def print(self, file):
        self.grep_file_content(file, expr="*")