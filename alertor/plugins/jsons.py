import json
import logging
        
logger = logging.getLogger('alertor')

class Plugin:
    def print_json(self, jsonfilepath):
        logger.info("Reading JSON file...")
        if jsonfilepath[-5:] == '.json':
            f = open(jsonfilepath, 'r')
            parsedjson = json.load(f)
            logger.info(json.dumps(parsedjson, indent=4))
        else:
            raise Exception("Wrong file format")

    def print(self, filename):
        self.print_json(filename)
