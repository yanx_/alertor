from scapy.all import *
import logging

logger = logging.getLogger('alertor')
        
class Plugin:
    def print_pcap(self, file: str, bpf: str):
        logger.info("Reading PCAP file...")

        if file[-5:] == '.pcap':
            sniff(offline=file, prn=lambda x: logger.info(x.summary()), filter=bpf, store=0)
        else:
            raise Exception("Wrong file format")

    def print(self, filename: str):
        self.print_pcap(filename, None)